// Load the TCP Library
net = require('net');

// Keep track of the chat clients
var clients = [];

var channels = {};

// cria um objeto de nicks
var nicks = {};

var users = {};

// Start a TCP Server
net.createServer(function (socket) {

  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort 

  // Put this new client in the list
  clients.push(socket);

  // Send a nice welcome message and announce
  socket.write("Bem vindo" + socket.name + "\n");
  broadcast(socket.name + " entrou no chat\n", socket);

  // Handle incoming messages from clients.
  socket.on('data', function (data) {
    // broadcast(socket.name + "> " + data, socket);
    // analisar a mensagem
    analisar(data);
  });

  // Remove the client from the list when it leaves
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " saiu do chat.\n");
  });
  
  // Send a message to all clients
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // Log it to the server output too
    process.stdout.write(message)
  }

  function analisar(data) {
    // visualizar os dados como são (sem tratamento)
    // console.log(data);
    // visualizar como string
    // console.log(String(data));
    // visualizar os caracteres especiais enviados
    // console.log(JSON.stringify(String(data)));
    // o método trim remove os caracteres especiais do final da string
    var mensagem = String(data).trim();
    // o método split quebra a mensagem em partes separadas p/ espaço em branco
    var args = mensagem.split(" "); 
    if ( args[0] == "NICK" ) nick(args); // se o primeiro argumento for NICK
    else if ( args[0] == "USER") user(args);
    else if ( args[0] == "JOIN") join(args);
    else if ( args[0] == "QUIT" ) quit(args);
    else socket.write("ERRO: comando inexistente\n");
  }
 
  function nick(args) {
    if(!args[1]){
    	socket.write("ERRO: nick está faltando\n");
    	return;
    }else if(nicks[ args[1]]){
    	socket.write("ERRO: nick já existe\n");
    	return;
    }else{
    	if(socket.nick){
    	//Verifica no vetor de nicks
        for(var i = 0; i< nicks.lenght-1; i++){
          if(nicks[i] == socket.nick)
          delete nicks[i];
        }
    	}
    //registra nicks no objeto
    nicks[args[1]] = socket;
    //registra o nickname na socket
    socket.nick = args[1];
    }
    socket.write("OK: comando NICK executado com sucesso\n");
  }

  function user(args) {
    var nome = "";

    if(!args[1]){
    	socket.write("ERRO: username está faltando\n");
    	return;
    }else if(!args[2]){
    	socket.write("ERRO: modo está faltando\n");
    	return;
    }else if(!args[3]){
    	socket.write("ERRO: terceiro argumento faltando\n");
    	return;
    }else if(!args[4]){
    	socket.write("ERRO: nome real está faltando\n");
    	return;
    }else{  
      for (var i = 0; i < args.length; i++) {
        if(args[i].charAt(0) == ":") {
            args[i] = args[i].replace(":", "")
            for (var j = i; j < args.length; j++) {
                nome += args[j];
                if(j + 1 < args.length) {
                    nome += " ";
                }
            }
        break;
        }
      }
    }

    users[args[1]] = {"user": args[1], "mode": args[2], "realname": nome, "socket": socket};
    socket.user = {"user": args[1], "mode": args[2], "realname": nome}
    socket.write("OK: comando USER executado com sucesso\n");
  }

  function join(args) {

    if (!args[1]) {
      socket.write("ERRO: estão faltando argumentos!\n");
    } else {
      var args_channel = args[1].split(',');
    }

    if (args[1] == '0') {
      for (var key in channels) {
        channels[key].users.forEach(function(c){
          if(c == users[socket.name]["user"]) {
            var index = channels[key].users.indexOf(c);
            channels[key].users.splice(index, 1);
          }
        });
      }
      socket.write("OK: Usuário saiu de todos os canais.\n");
    }
    
    if (args[2]) {
      var args_key = args[2].split(',');
    }

    args_channel.forEach(function(c){
      if (c in channels){
        if(c.users.includes(users[socket.name]["user"])){
          socket.write(`ERRO: Usuário já registrado no canal ${c}.\n`);
          return;
        } else {
          channels[c].push(users[socket.name]["user"]);
        }
      } else {
        channels[c] = { users: [], channelName: c};
        channels[c].users.push(users[socket.name]["user"]);
      }
      socket.write("OK: comando JOIN executado com sucesso\n");
    });
  }

  function part(args) {
    var args_msg = '';
    if (!args[1]) {
      socket.write("ERRO: estão faltando argumentos!\n");
      return;
    } else {
      var args_channel = args[1].split(',');
    }

    if (args[2]) {
      args_msg = args[2].replace(":", "");
    }

    args_channel.forEach(function(c){
      if(channels[c].users.includes(users[socket.name]["user"])) {
        var index = channels[c].clients.indexOf(users[socket.name]["user"]);
        channels[c].clients.splice(index, 1);
        socket.write(`OK: saiu do canal ${c}\n`);
      } else {
        socket.write(`ERRO: usuário não está no canal ${c}!\n`);
      }
    });
  }

  function privmsg(args) {
    if (!args[1] || !args[2]) {
      socket.write("ERRO: estão faltando argumentos!\n");
      return;
    } else {
      if( args[1] in nicks) {
        nicks[args[1]].socket.write(`${socket.nick}: ${args[2]}`);
      } else {
        socket.write(`ERRO: usuário com nick ${args[1]} não encontrado.`);
      }
    }
  }

  function quit(args) {
    delete nicks[socket.nick];
    socket.write(`${args[0]}\n`);
    socket.write("Até logo!\n");
    socket.destroy();
    return;
  }
}).listen(6667);

// Put a friendly message on the terminal of the server.
console.log("Server de Chat rodando na porta 6667\n");
