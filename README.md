# servidor IRC utilizando sockets em NODEJS


## P1 - Sistemas Distribuídos 2018/02

#### Membros:

- [Júlio Sousa](@juliosousa) *- Líder/Desenvolvedor*

- [Tiago Hermano](@tiagohermano) *- Desenvolvedor*

- [Vitor de Lima](@vitorlc) *- Documentação*

- [Denis Hideo Masunaga](@denismasunaga) *- Desenvolvedor*

- [Matheus de Assis](@matheus.dea) *- Documentação*

# Saiba Mais
**Acesse a documentação completa na [Wiki](https://gitlab.com/ad-si-2018-2/p1-g3/wikis/home).**
